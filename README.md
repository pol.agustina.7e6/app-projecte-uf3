# Projecte UF3
#### Versió: 1

### Descripció:

El projecte desenvolupat per na **Margaux Boileau** i en Pol Agustina per la unitat formativa tres del mòdul nou tracta d'una aplicació
per Android amb les millors obres d'art com a tema principal.
Dins l'aplicació l'usuari pot resgistrar-se, iniciar sessió i navegar entre el recull de les millors obres d'art creades pels millors 
artistes. L'usuari també pot convertir-se en artista si ho desitja i publicar les seves pròpies obres. I si té èxit entre els altres usuaris i s'afageixen les obres a les seves llistes es podrà fer famós i tot.
Tot això només és possible ja que l'aplicació, per un correcte funcionament, carrega les dades d'una API desenvolupada per Margaux i Pol. Aquesta obté les dades fent consultes a una base de dades que també han creat.

### Instal·lació i execució:

Per poder gaudir del producte final d'aquest projecte s'han de dur a terme certs procediments previs.

1. Disposar del programa **Intellij Idea**.

https://www.jetbrains.com/idea/download/#section=windows

2. Disposar del programa **Andoroid Studio**.

https://developer.android.com/studio

3. Disposar de **PostgreSQL**.

https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

Si ja en disposeu seguiu els següents passos per executar la API:

1. Obriu el contingut del repositori https://gitlab.com/pol.agustina.7e6/api-projecte-uf3.git amb **Intellij Idea**.

2. Veureu un arxiu anomenat "script.sql" al navegador de fitxers d'**Intellij Idea** situat a l'esquerra. Feu click dret sobre l'arxiu i copieu-ne la ruta absoluta.

3. Connecteu-vos a **PostgreSQL** i escriviu la següent comanda al promt:

        =# \i <enganxeu aquí la ruta absoluta del fitxer "script.sql">

4. Des del navegador del projecte obre el fitxer "Application.kt" del projecte situat
   a l'adreça "api-projecte-uf3/src/main/kotlin/com/example/Application.kt".

5. Executeu la funció **main**.

        NOTA: És vital conectar-se a PostgreSQL abans d'iniciar el producte final 
             del projecte per tenir un funcionament adecuat de l'aplicació

Un cop oberta la API només queda executar l'aplicació Android. Per executar-la heu de:

1. Obriu el contingut del repositori https://gitlab.com/pol.agustina.7e6/app-projecte-uf3.git amb Andoroid Studio.

2. Polsar el botó verd que hi ha al menú superior amb forma de fletxa (o apretar 'shift+F10').

        NOTA: Si compteu amb un dispositiu Android podeu connectar-lo al vostre ordinador
             i quan executeu l'aplicació s'us instal·larà directament al mòvil.
             

### Llicència:

MIT License

Copyright (c) [2023] [Margaux Boileau, Pol Agustina Prats]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
