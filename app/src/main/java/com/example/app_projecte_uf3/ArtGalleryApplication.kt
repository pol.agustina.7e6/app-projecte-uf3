package com.example.app_projecte_uf3

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArtGalleryApplication: Application() {
}