package com.example.app_projecte_uf3.data


import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.data.model.User
import com.example.app_projecte_uf3.data.network.ArtworksService
import com.example.app_projecte_uf3.data.network.ListsService
import com.example.app_projecte_uf3.data.network.UserService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Path
import javax.inject.Inject

class Repository @Inject constructor(
    private val artworksService: ArtworksService,
    private val userService: UserService,
    private val listsService: ListsService
) {
    //ARTWORKS
    suspend fun getAllArtworks(): List<Artwork>? {
        val response = artworksService.getAllArtworks()
        return response
    }
    suspend fun getArtworkById(url: String): Artwork? {
        val response = artworksService.getArtworkById(url)
        return response
    }
    suspend fun postArtwork(artwork: RequestBody, image: MultipartBody.Part):Int {
        val response = artworksService.postArtwork(artwork,image)
        return response
    }
    suspend fun putArtwork(url: String, artwork: RequestBody, image: MultipartBody.Part) {
        val response = artworksService.putArtwork(url,artwork,image)
        return response
    }
    suspend fun deleteArtwork(url: String) {
        val response = artworksService.deleteArtwork(url)
        return response
    }
    
    //USERS
    suspend fun login(user: User): User? {
        val response = userService.loginUser(user)
        return response
    }
    suspend fun getUserById(id: String): User? {
        val response = userService.getUserById(id)
        return response
    }
    suspend fun getUserLists(id: String): List<ArtworksList>?{
        val response = userService.getUserLists(id)
        return response
    }
    suspend fun getUserPosts(id: String): List<Artwork>?{
        val response = userService.getUserPosts(id)
        return response
    }
    suspend fun putUser(id: String, user: User):Int {
        val response = userService.putUser(id,user)
        return response
    }
    suspend fun postUser(user: User):Int {
        val response = userService.postUser(user)
        return response
    }
    suspend fun deleteUser(id: String) {
        val response = userService.deleteUser(id)
        return response
    }
    //LISTS
    suspend fun getListById(id: String): ArtworksList? {
        val response = listsService.getListById(id)
        return response
    }
    suspend fun postList(artworksList: ArtworksList) {
        val response = listsService.postList(artworksList)
        return response
    }
    suspend fun putList(id: String, artworksList: ArtworksList) {
        val response = listsService.putList(id,artworksList)
        return response
    }
    suspend fun deleteList( id: String) {
        val response = listsService.deleteList(id)
        return response
    }
    //ARTWORKS FROM LIST
    suspend fun getArtworksFromList(id: String): List<Artwork>? {
        val response = listsService.getArtworksFromList(id)
        return response
    }
    suspend fun postArtworkToList(idList: String, idArtwork: Int) {
        val response = listsService.postArtworkToList(idList,idArtwork)
        return response
    }
    suspend fun deleteArtworksFromList(idList: String, idArtwork: String) {
        val response = listsService.deleteArtworkFromList(idList,idArtwork)
        return response
    }
}