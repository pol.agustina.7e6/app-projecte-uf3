package com.example.app_projecte_uf3.data.model

data class Artwork(
    val idArtwork: Int,
    var name: String,
    var artist: String,
    var description: String,
    var image: String,
    var year: Int,
    var location: String,
    var genres: MutableList<String>,
    var idCreator:Int
)

