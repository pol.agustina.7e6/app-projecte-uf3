package com.example.app_projecte_uf3.data.model

data class ArtworksList(
    val idList: Int,
    val idUser: Int,
    var name: String,
    val size: Int,
    val creationDate: String
)
