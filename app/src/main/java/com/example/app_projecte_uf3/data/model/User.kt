package com.example.app_projecte_uf3.data.model

data class User(
    val idUser: Int,
    val name: String,
    val password: String,
)
