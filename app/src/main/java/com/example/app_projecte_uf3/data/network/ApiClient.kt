package com.example.app_projecte_uf3.data.network

import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.data.model.User
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface ApiClient {
    //ARTWORK ROUTES
    @GET("artworks")
    suspend fun getAllArtworks(): Response<List<Artwork>>
    @GET("artworks/{id}")
    suspend fun getArtworkById(@Path("id") url: String): Response<Artwork>
    @POST("artworks")
    @Multipart
    suspend fun postArtwork(@Part("artwork") artwork: RequestBody, @Part image: MultipartBody.Part): Response<ResponseBody>
    @PUT("artworks/{id}")
    @Multipart
    suspend fun putArtwork(@Path("id") url: String, @Part("artwork") artwork: RequestBody, @Part image: MultipartBody.Part): Response<ResponseBody>
    @DELETE("artworks/{id}")
    suspend fun deleteArtwork(@Path("id") url: String): Response<ResponseBody>

    //USER ROUTES
    @POST("users/login")
    suspend fun loginUser(@Body user: User): Response<User>
    @GET("users/{id}")
    suspend fun getUserById(@Path("id") url: String): Response<User>
    @GET("users/{id}/lists")
    suspend fun getUserLists(@Path("id") url: String): Response<List<ArtworksList>>
    @GET("users/{id}/posts")
    suspend fun getUserPosts(@Path("id") url: String): Response<List<Artwork>>
    @PUT("users/{id}")
    suspend fun putUser(@Path("id") url: String, @Body user: User): Response<ResponseBody>
    @POST("users")
    suspend fun postUser(@Body user: User): Response<ResponseBody>
    @DELETE("users/{id}")
    suspend fun deleteUser(@Path("id") url: String): Response<ResponseBody>

    //LISTS ROUTES
    @GET("lists/{id}")
    suspend fun getListById(@Path("id") url: String): Response<ArtworksList>
    @POST("lists")
    suspend fun postList(@Body artworksList: ArtworksList): Response<ResponseBody>
    @PUT("lists/{id}")
    suspend fun putList(@Path("id") url: String, @Body artworksList: ArtworksList): Response<ResponseBody>
    @DELETE("lists/{id}")
    suspend fun deleteList(@Path("id") url: String): Response<ResponseBody>

    @GET("lists/{id}/artworks")
    suspend fun getArtworksFromList(@Path("id") url: String): Response<List<Artwork>>
    @POST("lists/{id}/artworks")
    suspend fun postArtworkToList(@Path("id") url: String, @Body artworkId: Int): Response<ResponseBody>
    @DELETE("lists/{listId}/artworks/{artworkId}")
    suspend fun deleteArtworkFromList(@Path("listId") url1: String, @Path("artworkId") url2: String,): Response<ResponseBody>

}