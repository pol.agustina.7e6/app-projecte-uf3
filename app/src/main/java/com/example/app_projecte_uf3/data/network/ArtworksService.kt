package com.example.app_projecte_uf3.data.network

import com.example.app_projecte_uf3.data.model.Artwork
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Part
import java.io.File
import javax.inject.Inject

class ArtworksService @Inject constructor(private val api:ApiClient) {
    suspend fun getAllArtworks(): List<Artwork>? {
        return withContext(Dispatchers.IO) {
            val response = api.getAllArtworks()
            response.body()
        }
    }

    suspend fun getArtworkById(id: String): Artwork? {
        return withContext(Dispatchers.IO) {
            val response = api.getArtworkById(id)
            response.body()
        }
    }

    suspend fun postArtwork(artwork: RequestBody, image: MultipartBody.Part):Int {
        return withContext(Dispatchers.IO) {
            val response = api.postArtwork(artwork,image)
            response.code()
        }
    }

    suspend fun putArtwork(id: String, artwork: RequestBody, image: MultipartBody.Part) {
        return withContext(Dispatchers.IO) {
            val response = api.putArtwork(id, artwork, image)
            response.code()
        }
    }

    suspend fun deleteArtwork(id: String) {
        return withContext(Dispatchers.IO) {
            val response = api.deleteArtwork(id)
            response.code()
        }
    }
}