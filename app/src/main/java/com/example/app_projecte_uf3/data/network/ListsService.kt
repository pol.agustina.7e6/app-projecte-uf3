package com.example.app_projecte_uf3.data.network

import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ListsService @Inject constructor(private val api:ApiClient) {
    suspend fun getListById(id: String): ArtworksList? {
        return withContext(Dispatchers.IO) {
            val response = api.getListById(id)
            response.body()
        }
    }
    suspend fun postList(artworksList: ArtworksList) {
        return withContext(Dispatchers.IO) {
            val response = api.postList(artworksList)
            response.code()
        }
    }
    suspend fun putList(id: String, artworksList: ArtworksList) {
        return withContext(Dispatchers.IO) {
            val response = api.putList(id,artworksList)
            response.code()
        }
    }
    suspend fun deleteList( id: String) {
        return withContext(Dispatchers.IO) {
            val response = api.deleteList(id)
            response.code()
        }
    }

    suspend fun getArtworksFromList(id: String): List<Artwork>? {
        return withContext(Dispatchers.IO) {
            val response = api.getArtworksFromList(id)
            response.body()
        }
    }
    suspend fun postArtworkToList(idList: String, idArtwork: Int) {
        return withContext(Dispatchers.IO) {
            val response = api.postArtworkToList(idList,idArtwork)
            response.code()
        }
    }
    suspend fun deleteArtworkFromList(idList: String, idArtwork: String) {
        return withContext(Dispatchers.IO) {
            val response = api.deleteArtworkFromList(idList,idArtwork)
            response.code()
        }
    }

}