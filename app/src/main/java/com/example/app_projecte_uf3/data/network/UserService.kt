package com.example.app_projecte_uf3.data.network

import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.data.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserService @Inject constructor(private val api:ApiClient) {

    suspend fun loginUser(user: User):User?{
        return withContext(Dispatchers.IO) {
            val response = api.loginUser(user)
            response.body()
        }
    }

    suspend fun getUserById(id: String): User? {
        return withContext(Dispatchers.IO) {
            val response = api.getUserById(id)
            response.body()
        }
    }

    suspend fun getUserLists(id: String): List<ArtworksList>? {
        return withContext(Dispatchers.IO) {
            val response = api.getUserLists(id)
            response.body()
        }
    }

    suspend fun getUserPosts(id: String): List<Artwork>? {
        return withContext(Dispatchers.IO) {
            val response = api.getUserPosts(id)
            response.body()
        }
    }

    suspend fun putUser(id: String, user: User):Int {
        return withContext(Dispatchers.IO) {
            val response = api.putUser(id,user)
            response.code()
        }
    }

    suspend fun postUser(user: User):Int {
        return withContext(Dispatchers.IO) {
            val response = api.postUser(user)
            response.code()
        }
    }

    suspend fun deleteUser(id: String) {
        return withContext(Dispatchers.IO) {
            val response = api.deleteUser(id)
            response.code()
        }
    }
}