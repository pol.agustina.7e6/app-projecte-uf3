package com.example.app_projecte_uf3.ui.adapter

import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.Artwork
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app_projecte_uf3.databinding.ArtworkCardItemBinding
import com.example.app_projecte_uf3.utils.Constants


class ArtworksAdapterHoritzontal(private var artworksList:List<Artwork>, private val listener: OnClickArtworkListener): RecyclerView.Adapter<ArtworksAdapterHoritzontal.ArtworksViewHolder>() {

    inner class ArtworksViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ArtworkCardItemBinding.bind(view)
        fun setListener(artwork: Artwork){
            binding.actionBtn.setOnClickListener {
                listener.onItemClicked(artwork)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtworksViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ArtworksViewHolder(layoutInflater.inflate(R.layout.artwork_card_item, parent, false))
    }

    override fun onBindViewHolder(holder: ArtworksViewHolder, position: Int) {
        val item = artworksList[position]
        with(holder){
            binding.titleTv.text = item.name
            binding.artistTv.text = item.artist
            Glide.with(holder.binding.root)
                .load(Constants.ARTGALLERY_API_URL+item.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imgIv)
            setListener(item)
        }
    }

    override fun getItemCount(): Int = artworksList.size
}