package com.example.app_projecte_uf3.ui.adapter

import android.content.ContentValues.TAG
import android.util.Log
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.Artwork
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.databinding.ItemArtworkVerticalBinding
import com.example.app_projecte_uf3.utils.Constants


class ArtworksAdapterVertical(private val showMenu:Boolean,private var artworksList:List<Artwork>, private val listener: OnClickArtworkListener): RecyclerView.Adapter<ArtworksAdapterVertical.ArtworksViewHolder>() {

    inner class ArtworksViewHolder(view: View): RecyclerView.ViewHolder(view){

        val binding = ItemArtworkVerticalBinding.bind(view)

        fun setListener(artwork: Artwork){
            binding.root.setOnClickListener {
                listener.onItemClicked(artwork)
            }
            if (!showMenu) binding.icMore.visibility = View.INVISIBLE
            binding.icMore.setOnClickListener {
                val popupMenu = PopupMenu(it.context, binding.icMore)
                val inflater = popupMenu.menuInflater
                inflater.inflate(R.menu.menu_list_options, popupMenu.menu)
                popupMenu.menu.findItem(R.id.option_2).isVisible=false
                popupMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.option_1 -> {
                            listener.onDeleteClicked(artwork)
                            true
                        }
                        else -> false
                    }
                }
                popupMenu.show()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtworksViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ArtworksViewHolder(layoutInflater.inflate(R.layout.item_artwork_vertical, parent, false))
    }

    override fun onBindViewHolder(holder: ArtworksViewHolder, position: Int) {
        val item = artworksList[position]
        with(holder){
            binding.titleArtworkTvX.text = item.name
            binding.artistArtworkTvX.text = item.artist
            Glide.with(holder.binding.root)
                .load(Constants.ARTGALLERY_API_URL+item.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imgArtworkIvX)
            setListener(item)
        }
    }

    override fun getItemCount(): Int = artworksList.size

    fun updateList(newList: List<Artwork>) {
        this.artworksList = newList
        notifyDataSetChanged()
    }
}