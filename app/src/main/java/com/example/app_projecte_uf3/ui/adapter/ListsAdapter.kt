package com.example.app_projecte_uf3.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.databinding.ItemListBinding

class ListsAdapter(private var lists:List<ArtworksList>, private val listener: OnClickListListener): RecyclerView.Adapter<ListsAdapter.ListsViewHolder>() {

    inner class ListsViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemListBinding.bind(view)
        fun setListener(list: ArtworksList){
            binding.root.setOnClickListener {
                listener.onItemClicked(list)
            }
            binding.icMore.setOnClickListener {
                val popupMenu = PopupMenu(it.context, binding.icMore)
                val inflater = popupMenu.menuInflater
                inflater.inflate(R.menu.menu_list_options, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.option_1 -> {
                            listener.onDeleteClicked(list.idList)
                            true
                        }
                        R.id.option_2 -> {
                            listener.onEditClicked(list)
                            true
                        }
                        else -> false
                    }
                }
                popupMenu.show()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListsViewHolder(layoutInflater.inflate(R.layout.item_list, parent, false))
    }

    override fun onBindViewHolder(holder: ListsViewHolder, position: Int) {
        val item = lists[position]
        with(holder){
            binding.titleListTv.text = item.name
            setListener(item)
        }
    }

    override fun getItemCount(): Int = lists.size

    fun updateList(newList: List<ArtworksList>) {
        this.lists = newList
        notifyDataSetChanged()
    }
}