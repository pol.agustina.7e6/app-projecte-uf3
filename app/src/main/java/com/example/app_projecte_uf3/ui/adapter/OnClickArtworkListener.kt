package com.example.app_projecte_uf3.ui.adapter

import com.example.app_projecte_uf3.data.model.Artwork

interface OnClickArtworkListener {
    fun onItemClicked(artwork: Artwork)
    fun onDeleteClicked(artwork: Artwork)
}