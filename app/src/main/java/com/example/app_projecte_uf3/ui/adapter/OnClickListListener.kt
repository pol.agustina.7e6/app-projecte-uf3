package com.example.app_projecte_uf3.ui.adapter

import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList

interface OnClickListListener {
    fun onItemClicked(artworksList:ArtworksList)
    fun onDeleteClicked(idList:Int)
    fun onEditClicked(artworksList:ArtworksList)
}