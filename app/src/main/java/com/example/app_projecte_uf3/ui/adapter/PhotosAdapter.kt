package com.example.app_projecte_uf3.ui.adapter

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.ItemImageBinding
import com.example.app_projecte_uf3.utils.Constants


class PhotosAdapter(private var artworksList:List<Artwork>, private val listener: OnClickArtworkListener) :
    RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder>() {

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemImageBinding.bind(view)
        fun setListener(artwork: Artwork){
            binding.root.setOnClickListener {
                listener.onItemClicked(artwork)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PhotoViewHolder(layoutInflater.inflate(R.layout.item_image, parent, false))
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val item = artworksList[position]
        with(holder) {
            Glide.with(holder.binding.root)
                .load(Constants.ARTGALLERY_API_URL+item.image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.imageView4)
            setListener(item)
        }
    }

    override fun getItemCount(): Int = artworksList.size
}