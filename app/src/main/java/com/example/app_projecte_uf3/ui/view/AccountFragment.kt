package com.example.app_projecte_uf3.ui.view

import android.graphics.Rect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.DialogCreationFullBinding
import com.example.app_projecte_uf3.databinding.DialogNewListBinding
import com.example.app_projecte_uf3.databinding.FragmentAccountBinding
import com.example.app_projecte_uf3.ui.adapter.ArtworksAdapterVertical
import com.example.app_projecte_uf3.ui.adapter.OnClickArtworkListener
import com.example.app_projecte_uf3.ui.adapter.PhotosAdapter
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import com.example.app_projecte_uf3.utils.TempProvider
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import java.io.File

@AndroidEntryPoint
class AccountFragment : Fragment(), OnClickArtworkListener {

    var artworksList = emptyList<Artwork>()
    lateinit var binding: FragmentAccountBinding
    lateinit var userViewModel: UserViewModel
    lateinit var artworkViewModel: ArtworkViewModel
    lateinit var listsViewModel: ListsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAccountBinding.inflate(inflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        listsViewModel = ViewModelProvider(requireActivity())[ListsViewModel::class.java]
        artworkViewModel.getUserPosts(userViewModel.user.value!!.idUser)
        listsViewModel.getUserLists(userViewModel.user.value!!.idUser)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        println(listsViewModel.userLists.value)

        artworkViewModel.userPosts.observe(viewLifecycleOwner){
            artworksList = it!!
            setuprecyclerView()
            binding.postsNum.text=artworksList.size.toString()
        }
        listsViewModel.userLists.observe(viewLifecycleOwner){
            binding.listsNum.text=listsViewModel.userLists.value!!.size.toString()
        }

        binding.username.text=userViewModel.user.value!!.name

    }

    private fun setuprecyclerView() {
        val myAdapter = PhotosAdapter(artworksList,this)
        binding.recyclerImages.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(requireActivity(),3)
            adapter = myAdapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                    outRect.set(0, 0, 0, 0) // Set spacing to 0
                }
            })
        }
    }

    override fun onItemClicked(artwork: Artwork) {
        artworkViewModel.artworkSelected.value = artwork
        val action = AccountFragmentDirections.actionAccountFragmentToDetailFragment()
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onDeleteClicked(artwork: Artwork) {

    }
}