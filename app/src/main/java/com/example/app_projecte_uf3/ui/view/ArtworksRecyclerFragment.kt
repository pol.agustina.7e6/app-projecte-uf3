package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.FragmentArtworksRecyclerBinding
import com.example.app_projecte_uf3.ui.adapter.ArtworksAdapterVertical
import com.example.app_projecte_uf3.ui.adapter.OnClickArtworkListener
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.utils.TempProvider
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArtworksRecyclerFragment : Fragment(),OnClickArtworkListener {

    var artworks = emptyList<Artwork>()
    lateinit var listsViewModel: ListsViewModel
    lateinit var binding: FragmentArtworksRecyclerBinding
    lateinit var artworkViewModel:ArtworkViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentArtworksRecyclerBinding.inflate(layoutInflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        listsViewModel = ViewModelProvider(requireActivity())[ListsViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listsViewModel.artworksFromList.observe(viewLifecycleOwner) {
            if (it!=null){
                artworks = it!!
                setuprecyclerView()
            }
        }
    }

    private fun setuprecyclerView() {
        val myAdapter = ArtworksAdapterVertical(true, artworks, this)
        binding.recyclerArtworksY.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL,false)
            adapter = myAdapter
        }
    }

    override fun onItemClicked(artwork: Artwork) {
        println(artwork.name)
        artworkViewModel.artworkSelected.value = artwork
        val action = ArtworksRecyclerFragmentDirections.actionArtworksRecyclerFragmentToDetailFragment()
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onDeleteClicked(artwork:Artwork) {
        listsViewModel.deleteArtworkFromList(listsViewModel.listSelected.value!!.idList,artwork.idArtwork)
        listsViewModel.getArtworksFromList(listsViewModel.listSelected.value!!.idList)
    }
}