package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.databinding.FragmentDetailBinding
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import com.example.app_projecte_uf3.utils.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    lateinit var artworkViewModel: ArtworkViewModel
    lateinit var userViewModel: UserViewModel
    lateinit var listsViewModel: ListsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        listsViewModel = ViewModelProvider(requireActivity())[ListsViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.artistTvDetail.text = artworkViewModel.artworkSelected.value!!.artist
        binding.descriptionTvDetail.text = artworkViewModel.artworkSelected.value!!.description
        binding.yearInfoTvDetail.text = artworkViewModel.artworkSelected.value!!.year.toString()
        binding.locationInfoTvDetail.text = artworkViewModel.artworkSelected.value!!.location
        Glide.with(binding.root)
            .load(Constants.ARTGALLERY_API_URL+artworkViewModel.artworkSelected.value!!.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(binding.imageView)
        listsViewModel.getUserLists(userViewModel.user.value!!.idUser)

        listsViewModel.userLists.observe(viewLifecycleOwner) {
            val items = it.map { it.name }.toTypedArray()
            val builder = MaterialAlertDialogBuilder(requireContext())
            builder.setTitle("List Dialog")
            builder.setItems(items) { dialog, which ->
                val selectedItem = items[which]
                val findId = listsViewModel.userLists.value!!.filter { it.name==selectedItem }
                //GETARTWORKS FROM THIS LIST
                //IF SELECTED ITEM IN THIS LIST -> TOAST ALREADY IN THIS LIST
                // ELSE -> ADD THIS ARTWORK TO THE LIST SELECTED & SNACKBAR
                val message = "Artwork added to list"
                val actionText = "See more"
                val snackbar = Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG)
                snackbar.setAction(actionText) {
                    val action = DetailFragmentDirections.actionDetailFragmentToUserListsFragment()
                    Navigation.findNavController(binding.root).navigate(action)
                }
                if(findId[0] in listsViewModel.userLists.value!!) {
                    listsViewModel.postArtworkToList(findId[0].idList,artworkViewModel.artworkSelected.value!!.idArtwork)
                    snackbar.show()
                }
                else {
                    Toast.makeText(requireActivity(), "Artwork already in list", Toast.LENGTH_SHORT).show()
                }

            }
            builder.setNegativeButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            builder.setNeutralButton("Create list") { dialog, which ->
                listsViewModel.actionCreateList.value = true
                dialog.dismiss()
                val action = DetailFragmentDirections.actionDetailFragmentToUserListsFragment()
                Navigation.findNavController(binding.root).navigate(action)
            }

            binding.topAppBar.setNavigationOnClickListener {
                requireActivity().onBackPressed()
            }
            binding.topAppBar.setTitle(artworkViewModel.artworkSelected.value!!.name)
            binding.topAppBar.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.more -> {
                        val popupMenu = PopupMenu(requireContext(), binding.topAppBar.findViewById(R.id.more))
                        val inflater = popupMenu.menuInflater
                        inflater.inflate(R.menu.menu_overflow_detail, popupMenu.menu)
                        popupMenu.setOnMenuItemClickListener { item ->
                            when (item.itemId) {
                                R.id.option_1 -> {
                                    //SHOW DIALOG
                                    builder.show()
                                    true
                                }
                                else -> false
                            }
                        }
                        // SHOW MENU
                        popupMenu.show()
                        true
                    }
                    else -> false
                }
            }
        }
    }
}