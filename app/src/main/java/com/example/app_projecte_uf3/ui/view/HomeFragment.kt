package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.FragmentHomeBinding
import com.example.app_projecte_uf3.ui.adapter.ArtworksAdapterHoritzontal
import com.example.app_projecte_uf3.ui.adapter.OnClickArtworkListener
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), OnClickArtworkListener {

    lateinit var binding: FragmentHomeBinding
    lateinit var artworkViewModel:ArtworkViewModel
    lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        artworkViewModel.allArtworks.observe(viewLifecycleOwner) {
            setupRecycler1(artworkViewModel.filterArtworksByGenre("oleo"))
            setupRecycler2(artworkViewModel.filterArtworksByGenre("renacimiento"))
        }
    }
    private fun setupRecycler1(list: List<Artwork>) {
        val myAdapter = ArtworksAdapterHoritzontal(list, this)
        binding.recyclerArtworksX1.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false)
            adapter = myAdapter
        }
    }
    private fun setupRecycler2(list: List<Artwork>) {
        val myAdapter = ArtworksAdapterHoritzontal(list, this)
        binding.recyclerArtworksX2.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false)
            adapter = myAdapter
        }
    }
    override fun onItemClicked(artwork: Artwork) {
        artworkViewModel.artworkSelected.value = artwork
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment()
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onDeleteClicked(artwork: Artwork) {
    }
}