package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.databinding.DialogNewListBinding
import com.example.app_projecte_uf3.databinding.FragmentListsRecyclerBinding
import com.example.app_projecte_uf3.ui.adapter.ListsAdapter
import com.example.app_projecte_uf3.ui.adapter.OnClickListListener
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ListsRecyclerFragment : Fragment(),OnClickListListener,
    android.widget.SearchView.OnQueryTextListener {

    var lists = emptyList<ArtworksList>()
    var myAdapter = ListsAdapter(lists, this)
    lateinit var binding: FragmentListsRecyclerBinding
    lateinit var listsViewModel: ListsViewModel
    lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        listsViewModel = ViewModelProvider(requireActivity())[ListsViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentListsRecyclerBinding.inflate(inflater, container, false)

        listsViewModel.getUserLists(userViewModel.user.value!!.idUser)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val builder = MaterialAlertDialogBuilder(requireContext())
        val dialogBinding = DialogNewListBinding.inflate(layoutInflater)
        builder.setTitle("New list name:")
        builder.setPositiveButton("Save") { dialog, which ->
            val listName = dialogBinding.editText.text.toString()
            if (listsViewModel.actionCreateList.value!!){
                val newList = ArtworksList(0,userViewModel.user.value!!.idUser,listName, 0, Date().toString())
                listsViewModel.postList(newList)
                listsViewModel.actionCreateList.value = false
            }
            else if (listsViewModel.actionUpdateList.value!!) {
                listsViewModel.listSelected.value!!.name = listName
                listsViewModel.putList(listsViewModel.listSelected.value!!)
                listsViewModel.actionUpdateList.value = false
            }
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
            listsViewModel.actionCreateList.value = false
            dialog.dismiss()
        }
        builder.setView(dialogBinding.root)

        listsViewModel.userLists.observe(viewLifecycleOwner, Observer {
            lists = it
            setuprecyclerView()
        })

        listsViewModel.actionCreateList.observe(viewLifecycleOwner, Observer {
            if (it) {
                builder.show()
            }
        })
        listsViewModel.actionUpdateList.observe(viewLifecycleOwner, Observer {
            if (it) {
                builder.show()
            }
        })

        binding.searchviewLists.setOnQueryTextListener(this)
        binding.addFab.setOnClickListener{
            listsViewModel.actionCreateList.value=true
        }
    }

    private fun setuprecyclerView() {
        myAdapter = ListsAdapter(lists, this)
        binding.recyclerLists.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL,false)
            adapter = myAdapter
        }

    }

    override fun onItemClicked(artworksList: ArtworksList) {
        listsViewModel.getArtworksFromList(artworksList.idList)
        listsViewModel.listSelected.value = artworksList
        val action = ListsRecyclerFragmentDirections.actionUserListsFragmentToArtworksRecyclerFragment()
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onDeleteClicked(idList:Int) {
        listsViewModel.deleteList(idList, userViewModel.user.value!!.idUser!!)
    }

    override fun onEditClicked(artworksList: ArtworksList) {
        //Toast.makeText(requireActivity(),"Edit", Toast.LENGTH_SHORT).show()
        listsViewModel.listSelected.value = artworksList
        listsViewModel.actionUpdateList.value=true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        val filteredList = lists.filter { it.name.contains(query.toString(), ignoreCase = true)}
        myAdapter.updateList(filteredList)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val filteredList = lists.filter { it.name.contains(newText.toString(), ignoreCase = true)}
        myAdapter.updateList(filteredList)
        return true
    }
}