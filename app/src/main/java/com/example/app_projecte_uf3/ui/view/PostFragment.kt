package com.example.app_projecte_uf3.ui.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.FragmentArtworksRecyclerBinding
import com.example.app_projecte_uf3.databinding.FragmentPostBinding
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import com.example.app_projecte_uf3.utils.TempProvider
import java.io.File
import java.io.FileOutputStream

class PostFragment : Fragment() {
    lateinit var binding: FragmentPostBinding
    lateinit var artworkViewModel: ArtworkViewModel
    lateinit var userViewModel: UserViewModel
    var file: File? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPostBinding.inflate(layoutInflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]

        binding.topAppBarCreation.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        binding.topAppBarCreation.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.save -> {
                    val name = binding.titleEt.text.toString()
                    val artist = binding.artistEt.text.toString()
                    val description = binding.descriptionEt.text.toString()
                    val year = binding.year.text.toString().toInt()
                    val location = binding.locationEt.text.toString()
                    val genres = mutableListOf<String>()
                    val artwork = Artwork(0,name, artist, description, "",year,location, genres, userViewModel.user.value!!.idUser)
                    if (file!=null){
                        artworkViewModel.postArtwork(artwork, file!!)
                        requireActivity().onBackPressed()
                    }
                    else {
                        Toast.makeText(requireActivity(), "You need to upload an image", Toast.LENGTH_SHORT).show()
                    }
                    true
                }
                else-> false
                }
            }

        binding.uploadIv.setOnClickListener {
            if(ContextCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 10)
            }
            pickmedia.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
        }

        return binding.root
    }
    val pickmedia = registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
        if (uri != null) {
            binding.uploadIv.setImageURI(uri)
            println(uri)
            val projection = arrayOf(MediaStore.Images.Media.DISPLAY_NAME)
            val cursor = requireContext().contentResolver.query(uri, projection, null, null, null)
            cursor?.use {
                if (it.moveToFirst()) {
                    val columnIndex = it.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
                    val fileName = it.getString(columnIndex)
                    file = File(requireContext().cacheDir, fileName)
                    file!!.createNewFile()
                    val inputStream = requireContext().contentResolver.openInputStream(uri)
                    val outputStream = FileOutputStream(file)
                    inputStream?.copyTo(outputStream)
                } else {
                    // handle error - cursor is empty
                }
            }
        }
    }
}