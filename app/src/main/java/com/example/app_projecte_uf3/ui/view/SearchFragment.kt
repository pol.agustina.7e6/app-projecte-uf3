package com.example.app_projecte_uf3.ui.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView.OnQueryTextListener
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.databinding.FragmentSearchBinding
import com.example.app_projecte_uf3.ui.adapter.ArtworksAdapterVertical
import com.example.app_projecte_uf3.ui.adapter.ListsAdapter
import com.example.app_projecte_uf3.ui.adapter.OnClickArtworkListener
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.utils.TempProvider
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.io.FileOutputStream

@AndroidEntryPoint
class SearchFragment : Fragment(), OnClickArtworkListener, OnQueryTextListener {
    var allArtworks = emptyList<Artwork>()
    var myAdapter = ArtworksAdapterVertical(false, allArtworks, this)
    lateinit var binding: FragmentSearchBinding
    lateinit var artworkViewModel: ArtworkViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        artworkViewModel = ViewModelProvider(requireActivity())[ArtworkViewModel::class.java]
        artworkViewModel.getAllArtworks()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        artworkViewModel.allArtworks.observe(viewLifecycleOwner) {
            allArtworks = it!!
            setuprecyclerView()
        }
        binding.searchviewArtworks.setOnQueryTextListener(this)

    }

    private fun setuprecyclerView() {
        myAdapter = ArtworksAdapterVertical(false,allArtworks, this)
        binding.recyclerAllArtworks.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL,false)
            adapter = myAdapter
        }
    }

    override fun onItemClicked(artwork: Artwork) {
        artworkViewModel.artworkSelected.value = artwork
        val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment()
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onDeleteClicked(artwork: Artwork) {
        TODO("Not yet implemented")
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        val filteredList = allArtworks.filter { it.name.contains(query.toString(), ignoreCase = true)}
        myAdapter.updateList(filteredList)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val filteredList = allArtworks.filter { it.name.contains(newText.toString(), ignoreCase = true)}
        myAdapter.updateList(filteredList)
        return true
    }

}