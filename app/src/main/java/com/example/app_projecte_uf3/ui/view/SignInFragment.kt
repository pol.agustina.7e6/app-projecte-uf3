package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.User
import com.example.app_projecte_uf3.databinding.FragmentSignInBinding
import com.example.app_projecte_uf3.di.NetworkModule.provideRetrofit
import com.example.app_projecte_uf3.ui.viewmodel.ArtworkViewModel
import com.example.app_projecte_uf3.ui.viewmodel.ListsViewModel
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Inject

@AndroidEntryPoint
class SignInFragment : Fragment() {

    lateinit var binding: FragmentSignInBinding
    lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentSignInBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.goToRgstrBtn.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
        }

        userViewModel.user.observe(viewLifecycleOwner){
            if(it!=null) {
                findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
            }
            else{
                Toast.makeText(requireActivity(),"Wrong credentials",Toast.LENGTH_SHORT).show()
                binding.lgnName.setText("")
                binding.lgnPswrd.setText("")
            }
        }

        binding.lgnBtn.setOnClickListener {
            val name = binding.lgnName.text.toString()
            val password = binding.lgnPswrd.text.toString()
            userViewModel.login(User(0,name,password))
        }
    }
}