package com.example.app_projecte_uf3.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.app_projecte_uf3.R
import com.example.app_projecte_uf3.data.model.User
import com.example.app_projecte_uf3.databinding.FragmentSignUpBinding
import com.example.app_projecte_uf3.ui.viewmodel.UserViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : Fragment() {


    lateinit var binding: FragmentSignUpBinding
    lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        userViewModel = ViewModelProvider(requireActivity())[UserViewModel::class.java]
        binding = FragmentSignUpBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userViewModel.user.observe(viewLifecycleOwner){
            if(it!=null){
                findNavController().navigate(R.id.action_signUpFragment_to_homeFragment)
            }
            else{
                Toast.makeText(requireActivity(),"Username already in use",Toast.LENGTH_SHORT).show()
                binding.rgstrName.setText("")
                binding.rgstrPswrd.setText("")
            }
        }

        binding.rgstrBtn.setOnClickListener {
            val name = binding.rgstrName.text.toString()
            val password = binding.rgstrPswrd.text.toString()
            userViewModel.signUp(User(0,name,password))
        }
/*
        binding.rgstrBtn.setOnClickListener {

            if (binding.rgstrName.text.toString()=="") true

            fun signUp(name:String, password:String):Boolean {
                if (name=="" || password=="") return false

            }

            //poso if true perque no et peti la app pero sha de canviar amb crides a la bbdd i eso



            //username ja existeix
            if(true){
                Toast.makeText(requireActivity(),"Username not available", Toast.LENGTH_SHORT).show()
                binding.rgstrName.setText("")
                binding.rgstrPswrd.setText("")
                binding.rgstrEmail.setText("")
            }else if(true){
                binding.errorMsg.text = "Passwords don't match"
                binding.userpassword.setText("")
                binding.userrepeatpassword.setText("")
            //les passwords no son iguals
            //si vols fer que hi hagi un minim de caracters i que hi hagi numeros i eso va aqui
            } else {
                binding.errorMsg.text = ""
                //insertar user a la bbdd
                findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
            }
        }*/
    }

}