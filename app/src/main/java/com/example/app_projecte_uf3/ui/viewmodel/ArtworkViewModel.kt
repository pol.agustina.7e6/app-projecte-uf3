package com.example.app_projecte_uf3.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app_projecte_uf3.data.Repository
import com.example.app_projecte_uf3.data.model.Artwork
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ArtworkViewModel @Inject constructor(
    private val repository: Repository
): ViewModel() {
    var allArtworks = MutableLiveData<List<Artwork>?>()
    var artworkSelected = MutableLiveData<Artwork>()
    var userPosts = MutableLiveData<List<Artwork>?>()

    init {
        getAllArtworks()
    }

    fun getAllArtworks() {
        viewModelScope.launch {
            allArtworks.value = repository.getAllArtworks()
            println(allArtworks.value)
        }
    }
    fun getUserPosts(id:Int) {
        viewModelScope.launch {
            try {
                userPosts.value = repository.getUserPosts(id.toString())
            } catch (e: Exception){
                println("Error: ${e.message}")
            }
        }
    }

    fun getArtworkById() {
        viewModelScope.launch {
            repository.getArtworkById("1")
        }
    }
    fun deleteArtwork() {
        viewModelScope.launch {
            repository.deleteArtwork("1")
        }
    }
    fun postArtwork(artwork: Artwork, image: File) {
        val requestFile = image.asRequestBody("image/*".toMediaTypeOrNull())
        val imagePart = MultipartBody.Part.createFormData("image", image.name, requestFile)
        val gson = Gson()
        val artworkJson = gson.toJson(artwork)
        val artworkRequestBody = artworkJson.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        viewModelScope.launch {
            repository.postArtwork(artworkRequestBody,imagePart)
            getUserPosts(artwork.idCreator)
        }
    }
    fun putArtwork(id:Int, artwork: Artwork, image: File) {
        viewModelScope.launch {
            //repository.putArtwork()
        }
    }

    fun filterArtworksByGenre(genre:String): List<Artwork> {
        val filteredList = allArtworks.value!!.filter { it.genres.contains(genre)}
        return filteredList
    }
}