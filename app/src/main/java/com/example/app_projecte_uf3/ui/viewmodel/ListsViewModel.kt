package com.example.app_projecte_uf3.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app_projecte_uf3.data.Repository
import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.utils.TempProvider
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import javax.inject.Inject

@HiltViewModel
class ListsViewModel @Inject constructor(
    private val repository: Repository
): ViewModel() {
    var artworksFromList = MutableLiveData<List<Artwork>?>()
    var actionCreateList = MutableLiveData<Boolean>()
    var actionUpdateList = MutableLiveData<Boolean>()
    var listSelected = MutableLiveData<ArtworksList>()

    var userLists = MutableLiveData<List<ArtworksList>>()

    init {
        actionCreateList.value=false
        actionUpdateList.value=false
    }

    fun getUserLists(id:Int) {
        viewModelScope.launch {
            try {
                userLists.value = repository.getUserLists(id.toString())
            } catch (e: Exception){
                println("Error: ${e.message}")
            }
        }
    }

    fun postList(artworksList: ArtworksList){
        viewModelScope.launch {
            repository.postList(artworksList)
            getUserLists(artworksList.idUser)
        }
    }
    fun putList(artworksList: ArtworksList){
        viewModelScope.launch {
            repository.putList(artworksList.idList.toString(), artworksList)
            getUserLists(artworksList.idUser)
        }
    }
    fun deleteList(idList: Int, idUser:Int){
        viewModelScope.launch {
            repository.deleteList(idList.toString())
            getUserLists(idUser)
        }
    }
    fun getArtworksFromList(idList:Int) {
        viewModelScope.launch {
            try {
                artworksFromList.value = repository.getArtworksFromList(idList.toString())
            } catch (e: Exception){
                artworksFromList.value = emptyList()
                println("Error: ${e.message}")
            }
        }
    }
    fun postArtworkToList(idList: Int, idArtwork: Int){
        viewModelScope.launch {
            repository.postArtworkToList(idList.toString(),idArtwork)
        }
    }
    fun deleteArtworkFromList(idList: Int, idArtwork: Int){
        viewModelScope.launch {
            repository.deleteArtworksFromList(idList.toString(),idArtwork.toString())
            getArtworksFromList(idList)
        }
    }
}