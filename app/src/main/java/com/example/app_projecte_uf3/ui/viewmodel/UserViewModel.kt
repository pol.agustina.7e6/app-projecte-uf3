package com.example.app_projecte_uf3.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.app_projecte_uf3.data.Repository
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.data.model.User
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class UserViewModel @Inject constructor(
    private val repository: Repository
): ViewModel() {
    var user = MutableLiveData<User?>()

    init {
        //user.value=null
    }

    //FUNCIONA
    fun getUserById(id:Int) {
        viewModelScope.launch {
            println("GETING USER BY ID")
            user.value =  repository.getUserById(id.toString())
            println(user.value)
        }
    }
    fun deleteUser(idUser:Int) {
        viewModelScope.launch {
            repository.deleteUser(idUser.toString())
        }
    }
    fun postUser(user:User) {
        viewModelScope.launch {
            val x = repository.postUser(user)
            println(x)
        }
    }

    fun signUp(newUser:User):Boolean {
        if (newUser.name=="" || newUser.password=="") return false
        viewModelScope.launch {
            try {
                val x = repository.postUser(newUser)
                login(newUser)
            }catch (_:Exception){}
        }
        return user.value != null
    }

    fun login(newUser:User): Boolean {
        if (newUser.name=="" || newUser.password=="") return false
        viewModelScope.launch {
            println("GETING USER BY LOGIN")
            user.value =  repository.login(newUser)
            println(user.value)
        }
        return user.value != null
    }

    fun putUser(user:User) {
        viewModelScope.launch {
            repository.putUser("6",user)
        }
    }

}