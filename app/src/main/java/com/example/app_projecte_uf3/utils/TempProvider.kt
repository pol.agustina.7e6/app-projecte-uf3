package com.example.app_projecte_uf3.utils

import com.example.app_projecte_uf3.data.model.Artwork
import com.example.app_projecte_uf3.data.model.ArtworksList
import com.example.app_projecte_uf3.data.model.User

object TempProvider {
    val user = User(1, "Margaux", "1234")

    val listsList = listOf(
        ArtworksList(1,1,"Favourites", 5, "20-04-2023"),
        ArtworksList(2,1,"My artworks", 3, "20-04-2023"),
        ArtworksList(3,1,"Want to see", 15, "20-04-2023"),
        ArtworksList(4,2,"Gothic art", 7, "20-04-2023")
    )

    val artworksList = listOf(
        Artwork(1, "La gioconda", "Leonardo Da Vinci", "Description","images/shrek.jpg", 1503, "20-04-2023",mutableListOf("Barroque", "Portrait"),1),
        Artwork(2, "La gioconda", "Leonardo Da Vinci", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida massa non felis lobortis, non ullamcorper nulla auctor. Suspendisse quis purus lobortis, mollis leo nec, venenatis nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer eu nunc vitae arcu condimentum blandit et eu felis. Donec ut tempor lectus.","images/mario.png", 1503, "20-04-2023", mutableListOf("Barroque", "Portrait"),1),
        Artwork(2, "La gioconda", "Leonardo Da Vinci", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida massa non felis lobortis, non ullamcorper nulla auctor. Suspendisse quis purus lobortis, mollis leo nec, venenatis nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer eu nunc vitae arcu condimentum blandit et eu felis. Donec ut tempor lectus.","images/amapolas.jpg", 1503, "20-04-2023", mutableListOf("Barroque", "Portrait"),1)
    )
}